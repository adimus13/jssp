from setuptools import setup

setup(
    name = "jssp",
    packages = ["jssp"],
    entry_points = {
        "console_scripts": ['jssp = jssp.main:main']
        },
    version = "0.0.1",
    description = "JSSP - Simple python project manager",
    long_description = "...",
    author = "Michał Treter",
    author_email = "adimus13@gmail.com",
    url = "https://gitlab.com/adimus13/jssp",
    )